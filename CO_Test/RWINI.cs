﻿using System;
using System.Collections.Generic; 
using System.Text;  
using System.IO; 
using System.Collections;
using System.Runtime.InteropServices;

namespace MsgBox
{
    public class RWINI
    {
         private string filePath;
         
        [DllImport("kernel32")]
        private static extern long WritePrivateProfileString(string section,
        string key,
        string val,
        string filePath);
 
        [DllImport("kernel32")]
        private static extern int GetPrivateProfileString(string section,
        string key,
        string def,
        StringBuilder retVal,
        int size,
        string filePath);

        public RWINI(string filePath)
        {   
            ////Get the INI File Location
            this.filePath = filePath;  
        }
 
        public void Write(string section, string key, string value)
        {
            ////Write value to .INI
            WritePrivateProfileString(section, key, value.ToLower(), this.filePath);
        }
 
        public string Read(string section, string key)
        {   
            ////Read and get the Parameter value by provinding the Section [] and the Key of the parameter
            StringBuilder SB = new StringBuilder(255);
            int i = GetPrivateProfileString(section, key, "", SB, 255, this.filePath);
            return SB.ToString();
        }
         
        public string FilePath
        {
            get { return this.filePath; }
            set { this.filePath = value; }
        }
    }
}
