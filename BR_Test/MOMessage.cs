﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data.SqlClient;
using System.Data.SqlTypes;
using System.Data;
using System.Configuration;


namespace MsgBox
{
    public class MOMessage
    {
         
        public DataSet getMSG(BRProp BrObject, COMessage CoObject)
        {
            try
            {
                ////this will get the whitelist from database base on Listtype  
                DataSet ds = new DataSet();
                SqlConnection con = new SqlConnection(CoObject.strConString);

                using (System.Data.SqlClient.SqlCommand slqcommands = new System.Data.SqlClient.SqlCommand("sprGetMessage", con))
                {
                //slqcommands.Parameters.AddWithValue("@Seqno", BrObject.msgType);
                slqcommands.Parameters.AddWithValue("@DisplayType", BrObject.displayType);
                slqcommands.CommandTimeout = CoObject.SQLTimeout;
                    slqcommands.CommandType = CommandType.StoredProcedure;
                    SqlDataAdapter adp = new SqlDataAdapter(slqcommands);
                    adp.Fill(ds);
                }

                return ds;
            }
            catch (Exception)
            {
                throw;
            }
        }
    }
}
