﻿using System;
using System.Collections.Generic;
using System.Text;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Windows.Forms;
using System.Resources;
using System.IO;
using System.Reflection;
using System.Runtime.InteropServices;
using MsgBox;

namespace MsgBox
{
    public class MessageBOX
    {
        public Form MESSAGEForm = new Form();
        public Panel panel1 = new Panel();
        public PictureBox pctIcon = new PictureBox();
        public RichTextBox tbMessage = new RichTextBox();
        public Button btnOK = new Button();
        public Button btnSave = new Button();
        public Button btnCancel = new Button();
        public Button btnYes = new Button();
        public Button btnNo = new Button();
        public Button btnRetry = new Button();
        public Button btnAbort = new Button();
        public Button btnIgnore = new Button();
        public Button btnContinue = new Button();

        Assembly refAssembly = Assembly.LoadFrom("GenMSG.dll");

        private void MessageFORM_Load(object sender, EventArgs e)
        {
            MESSAGEForm.BringToFront();
        }
        private void showOK()
        {
            
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(MessageBOX));
            this.btnOK = new System.Windows.Forms.Button();
            this.tbMessage = new System.Windows.Forms.RichTextBox();
            this.pctIcon = new System.Windows.Forms.PictureBox();
            ((System.ComponentModel.ISupportInitialize)(this.pctIcon)).BeginInit();
            MESSAGEForm.SuspendLayout();

            //btnOK
            //btnOK.DialogResult = DialogResult.OK;
            btnOK.Location = new System.Drawing.Point(159, 133);
            btnOK.Name = "btnOK";
            btnOK.Size = new System.Drawing.Size(75, 23);
            btnOK.TabIndex = 0;
            btnOK.UseVisualStyleBackColor = true;
            btnOK.Text = "OK";
            btnOK.DialogResult = DialogResult.OK;

            //tbMessage
            tbMessage.BackColor = System.Drawing.Color.Gainsboro;
            tbMessage.BorderStyle = System.Windows.Forms.BorderStyle.None;
            tbMessage.Cursor = System.Windows.Forms.Cursors.Arrow;
            tbMessage.Font = new System.Drawing.Font("Gadugi", 9.5F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            tbMessage.Location = new System.Drawing.Point(90, 46);
            tbMessage.Size = new System.Drawing.Size(278, 56);
            tbMessage.Multiline = true;
            tbMessage.Name = "tbMessage";
            tbMessage.ReadOnly = true;
            tbMessage.ShortcutsEnabled = false;
            tbMessage.TabIndex = 2;
            tbMessage.SelectionAlignment = System.Windows.Forms.HorizontalAlignment.Center;
            tbMessage.DoubleClick += new EventHandler(tbMessage_DoubleClick);

            //pctIcon
            pctIcon.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Center;
            pctIcon.Location = new System.Drawing.Point(22, 43);
            pctIcon.Name = "pctIcon";
            pctIcon.Size = new System.Drawing.Size(56, 51);
            pctIcon.TabIndex = 3;
            pctIcon.TabStop = false;

            //MESSAGEForm
            MESSAGEForm.StartPosition = FormStartPosition.CenterScreen;
            
            MESSAGEForm.ShowIcon = true;
            MESSAGEForm.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            MESSAGEForm.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            MESSAGEForm.BackColor = System.Drawing.Color.Gainsboro;
            MESSAGEForm.ClientSize = new System.Drawing.Size(380, 168);
            MESSAGEForm.Controls.Add(pctIcon);
            MESSAGEForm.Controls.Add(tbMessage);
            MESSAGEForm.Controls.Add(btnOK);
            MESSAGEForm.ForeColor = System.Drawing.SystemColors.ControlText;
            MESSAGEForm.FormBorderStyle = FormBorderStyle.FixedDialog;
            MESSAGEForm.MaximizeBox = false;
            MESSAGEForm.MinimizeBox = false;
            MESSAGEForm.Name = "MESSAGEForm";
            ((System.ComponentModel.ISupportInitialize)(pctIcon)).EndInit();
            MESSAGEForm.ResumeLayout(false);
            MESSAGEForm.PerformLayout();

            MESSAGEForm.Load += new EventHandler(MessageFORM_Load);
        }

        private void showYesNo()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(MessageBOX));
            this.btnYes = new System.Windows.Forms.Button();
            this.btnNo = new System.Windows.Forms.Button();
            this.tbMessage = new System.Windows.Forms.RichTextBox();
            this.pctIcon = new System.Windows.Forms.PictureBox();
            ((System.ComponentModel.ISupportInitialize)(this.pctIcon)).BeginInit();
            MESSAGEForm.SuspendLayout();

            //btnYes
            btnYes.Location = new System.Drawing.Point(82, 133);
            btnYes.Name = "btnYES";
            btnYes.Size = new System.Drawing.Size(75, 23);
            btnYes.TabIndex = 0;
            btnYes.UseVisualStyleBackColor = true;
            btnYes.Text = "Yes";
            btnYes.DialogResult = DialogResult.Yes;

            //btnNo
            btnNo.Location = new System.Drawing.Point(224, 133);
            btnNo.Name = "btnNO";
            btnNo.Size = new System.Drawing.Size(75, 23);
            btnNo.TabIndex = 0;
            btnNo.UseVisualStyleBackColor = true;
            btnNo.Text = "No";
            btnNo.DialogResult = DialogResult.No;

            //tbMessage
            tbMessage.BackColor = System.Drawing.Color.Gainsboro;
            tbMessage.BorderStyle = System.Windows.Forms.BorderStyle.None;
            tbMessage.Cursor = System.Windows.Forms.Cursors.Arrow;
            tbMessage.Font = new System.Drawing.Font("Gadugi", 9.5F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            tbMessage.Location = new System.Drawing.Point(90, 46);
            tbMessage.Size = new System.Drawing.Size(278, 56);
            tbMessage.Multiline = true;
            tbMessage.Name = "tbMessage";
            tbMessage.ReadOnly = true;
            tbMessage.ShortcutsEnabled = false;
            tbMessage.TabIndex = 2;
            tbMessage.SelectAll();
            tbMessage.SelectionAlignment = System.Windows.Forms.HorizontalAlignment.Center;
            tbMessage.DeselectAll();
            tbMessage.DoubleClick += new EventHandler(tbMessage_DoubleClick);

            //pctIcon
            pctIcon.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Center;
            pctIcon.Location = new System.Drawing.Point(22, 43);
            pctIcon.Name = "pctIcon";
            pctIcon.Size = new System.Drawing.Size(56, 51);
            pctIcon.TabIndex = 3;
            pctIcon.TabStop = false;

            MESSAGEForm.StartPosition = FormStartPosition.CenterScreen;
           
            MESSAGEForm.ShowIcon = true;
            MESSAGEForm.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            MESSAGEForm.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            MESSAGEForm.BackColor = System.Drawing.Color.Gainsboro;
            MESSAGEForm.ClientSize = new System.Drawing.Size(380, 168);
            MESSAGEForm.Controls.Add(pctIcon);
            MESSAGEForm.Controls.Add(tbMessage);
            MESSAGEForm.Controls.Add(btnYes);
            MESSAGEForm.Controls.Add(btnNo);
            MESSAGEForm.ForeColor = System.Drawing.SystemColors.ControlText;
            MESSAGEForm.FormBorderStyle = FormBorderStyle.FixedDialog;
            MESSAGEForm.MaximizeBox = false;
            MESSAGEForm.MinimizeBox = false;
            MESSAGEForm.Name = "MESSAGEForm";
            ((System.ComponentModel.ISupportInitialize)(pctIcon)).EndInit();
            MESSAGEForm.ResumeLayout(false);
            MESSAGEForm.PerformLayout();

            
        }

        private void showYesNoCancel()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(MessageBOX));
            this.btnYes = new System.Windows.Forms.Button();
            this.btnNo = new System.Windows.Forms.Button();
            this.btnCancel = new System.Windows.Forms.Button();
            this.tbMessage = new System.Windows.Forms.RichTextBox();
            this.pctIcon = new System.Windows.Forms.PictureBox();
            ((System.ComponentModel.ISupportInitialize)(this.pctIcon)).BeginInit();
            MESSAGEForm.SuspendLayout();

            // 
            // btnYes
            // 
            btnYes.DialogResult = System.Windows.Forms.DialogResult.OK;
            btnYes.Location = new System.Drawing.Point(57, 133);
            btnYes.Name = "btnYes";
            btnYes.Size = new System.Drawing.Size(75, 23);
            btnYes.TabIndex = 0;
            btnYes.Text = "Yes";
            btnYes.UseVisualStyleBackColor = true;
            btnYes.DialogResult = DialogResult.Yes;

            // 
            // btnNo
            // 
            btnNo.Location = new System.Drawing.Point(159, 133);
            btnNo.Name = "btnNo";
            btnNo.Size = new System.Drawing.Size(75, 23);
            btnNo.TabIndex = 1;
            btnNo.Text = "No";
            btnNo.UseVisualStyleBackColor = true;
            btnNo.DialogResult = DialogResult.No;
            //btnRetry.Click += new System.EventHandler(this.button2_Click);

            // btnCancel
            // 
            btnCancel.Location = new System.Drawing.Point(262, 133);
            btnCancel.Name = "btnCancel";
            btnCancel.Size = new System.Drawing.Size(75, 23);
            btnCancel.TabIndex = 3;
            btnCancel.Text = "Cancel";
            btnCancel.UseVisualStyleBackColor = true;
            btnCancel.DialogResult = DialogResult.Cancel;

            //tbMessage
            tbMessage.BackColor = System.Drawing.Color.Gainsboro;
            tbMessage.BorderStyle = System.Windows.Forms.BorderStyle.None;
            tbMessage.Cursor = System.Windows.Forms.Cursors.Arrow;
            tbMessage.Font = new System.Drawing.Font("Gadugi", 9.5F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            tbMessage.Location = new System.Drawing.Point(90, 46);
            tbMessage.Size = new System.Drawing.Size(278, 56);
            tbMessage.Multiline = true;
            tbMessage.Name = "tbMessage";
            tbMessage.ReadOnly = true;
            tbMessage.ShortcutsEnabled = false;
            tbMessage.TabIndex = 2;
            tbMessage.SelectAll();
            tbMessage.SelectionAlignment = System.Windows.Forms.HorizontalAlignment.Center;
            tbMessage.DeselectAll();
            tbMessage.DoubleClick += new EventHandler(tbMessage_DoubleClick);

            //pctIcon
            pctIcon.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Center;
            pctIcon.Location = new System.Drawing.Point(22, 43);
            pctIcon.Name = "pctIcon";
            pctIcon.Size = new System.Drawing.Size(56, 51);
            pctIcon.TabIndex = 3;
            pctIcon.TabStop = false;

            MESSAGEForm.StartPosition = FormStartPosition.CenterScreen;

            MESSAGEForm.ShowIcon = true;
            MESSAGEForm.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            MESSAGEForm.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            MESSAGEForm.BackColor = System.Drawing.Color.Gainsboro;
            MESSAGEForm.ClientSize = new System.Drawing.Size(380, 168);
            MESSAGEForm.Controls.Add(pctIcon);
            MESSAGEForm.Controls.Add(tbMessage);
            MESSAGEForm.Controls.Add(btnYes);
            MESSAGEForm.Controls.Add(btnNo);
            MESSAGEForm.Controls.Add(btnCancel);
            MESSAGEForm.ForeColor = System.Drawing.SystemColors.ControlText;
            MESSAGEForm.FormBorderStyle = FormBorderStyle.FixedDialog;
            MESSAGEForm.MaximizeBox = false;
            MESSAGEForm.MinimizeBox = false;
            MESSAGEForm.Name = "MESSAGEForm";
            ((System.ComponentModel.ISupportInitialize)(pctIcon)).EndInit();
            MESSAGEForm.ResumeLayout(false);
            MESSAGEForm.PerformLayout();


        }

        private void showOKCancel()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(MessageBOX));
            this.btnOK = new System.Windows.Forms.Button();
            this.btnCancel = new System.Windows.Forms.Button();
            this.tbMessage = new System.Windows.Forms.RichTextBox();
            this.pctIcon = new System.Windows.Forms.PictureBox();
            ((System.ComponentModel.ISupportInitialize)(this.pctIcon)).BeginInit();
            MESSAGEForm.SuspendLayout();

            //btnOK
            btnOK.Location = new System.Drawing.Point(82, 133);
            btnOK.Name = "btnOK";
            btnOK.Size = new System.Drawing.Size(75, 23);
            btnOK.TabIndex = 0;
            btnOK.UseVisualStyleBackColor = true;
            btnOK.Text = "OK";
            btnOK.DialogResult = DialogResult.OK;

            //btnCancel
            btnCancel.Location = new System.Drawing.Point(224, 133);
            btnCancel.Name = "btnCancel";
            btnCancel.Size = new System.Drawing.Size(75, 23);
            btnCancel.TabIndex = 0;
            btnCancel.UseVisualStyleBackColor = true;
            btnCancel.Text = "Cancel";
            btnCancel.DialogResult = DialogResult.Cancel;
            //btnCancel.Click += new EventHandler(btnCancel_Click);

            //tbMessage
            tbMessage.BackColor = System.Drawing.Color.Gainsboro;
            tbMessage.BorderStyle = System.Windows.Forms.BorderStyle.None;
            tbMessage.Cursor = System.Windows.Forms.Cursors.Arrow;
            tbMessage.Font = new System.Drawing.Font("Gadugi", 9.5F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            tbMessage.Location = new System.Drawing.Point(90, 46);
            tbMessage.Size = new System.Drawing.Size(278, 56);
            tbMessage.Multiline = true;
            tbMessage.Name = "tbMessage";
            tbMessage.ReadOnly = true;
            tbMessage.ShortcutsEnabled = false;
            tbMessage.TabIndex = 2;
            tbMessage.SelectAll();
            tbMessage.SelectionAlignment = System.Windows.Forms.HorizontalAlignment.Center;
            tbMessage.DeselectAll();
            tbMessage.DoubleClick += new EventHandler(tbMessage_DoubleClick);

            //pctIcon
            pctIcon.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Center;
            pctIcon.Location = new System.Drawing.Point(22, 43);
            pctIcon.Name = "pctIcon";
            pctIcon.Size = new System.Drawing.Size(56, 51);
            pctIcon.TabIndex = 3;
            pctIcon.TabStop = false;

            MESSAGEForm.StartPosition = FormStartPosition.CenterScreen;
            
            MESSAGEForm.ShowIcon = true;
            MESSAGEForm.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            MESSAGEForm.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            MESSAGEForm.BackColor = System.Drawing.Color.Gainsboro;
            MESSAGEForm.ClientSize = new System.Drawing.Size(380, 168);
            MESSAGEForm.Controls.Add(pctIcon);
            MESSAGEForm.Controls.Add(tbMessage);
            MESSAGEForm.Controls.Add(btnOK);
            MESSAGEForm.Controls.Add(btnCancel);
            MESSAGEForm.ForeColor = System.Drawing.SystemColors.ControlText;
            MESSAGEForm.FormBorderStyle = FormBorderStyle.FixedDialog;
            MESSAGEForm.MaximizeBox = false;
            MESSAGEForm.MinimizeBox = false;
            MESSAGEForm.Name = "MESSAGEForm";
            ((System.ComponentModel.ISupportInitialize)(pctIcon)).EndInit();
            MESSAGEForm.ResumeLayout(false);
            MESSAGEForm.PerformLayout();
        }

        private void showSaveCancel()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(MessageBOX));
            this.btnSave = new System.Windows.Forms.Button();
            this.btnCancel = new System.Windows.Forms.Button();
            this.tbMessage = new System.Windows.Forms.RichTextBox();
            this.pctIcon = new System.Windows.Forms.PictureBox();
            ((System.ComponentModel.ISupportInitialize)(this.pctIcon)).BeginInit();
            MESSAGEForm.SuspendLayout();

            //btnSave
            btnSave.Location = new System.Drawing.Point(82, 133);
            btnSave.Name = "btnSave";
            btnSave.Size = new System.Drawing.Size(75, 23);
            btnSave.TabIndex = 0;
            btnSave.UseVisualStyleBackColor = true;
            btnSave.Text = "Save";
            btnSave.DialogResult = DialogResult.Yes;

            //btnCancel
            btnCancel.Location = new System.Drawing.Point(224, 133);
            btnCancel.Name = "btnCancel";
            btnCancel.Size = new System.Drawing.Size(75, 23);
            btnCancel.TabIndex = 0;
            btnCancel.UseVisualStyleBackColor = true;
            btnCancel.Text = "Cancel";
            btnCancel.DialogResult = DialogResult.Cancel;
            //btnCancel.Click += new EventHandler(btnCancel_Click);

            //tbMessage
            tbMessage.BackColor = System.Drawing.Color.Gainsboro;
            tbMessage.BorderStyle = System.Windows.Forms.BorderStyle.None;
            tbMessage.Cursor = System.Windows.Forms.Cursors.Arrow;
            tbMessage.Font = new System.Drawing.Font("Gadugi", 9.5F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            tbMessage.Location = new System.Drawing.Point(90, 46);
            tbMessage.Size = new System.Drawing.Size(278, 56);
            tbMessage.Multiline = true;
            tbMessage.Name = "tbMessage";
            tbMessage.ReadOnly = true;
            tbMessage.ShortcutsEnabled = false;
            tbMessage.TabIndex = 2;
            tbMessage.SelectAll();
            tbMessage.SelectionAlignment = System.Windows.Forms.HorizontalAlignment.Center;
            tbMessage.DeselectAll();
            tbMessage.DoubleClick += new EventHandler(tbMessage_DoubleClick);

            //pctIcon
            pctIcon.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Center;
            pctIcon.Location = new System.Drawing.Point(22, 43);
            pctIcon.Name = "pctIcon";
            pctIcon.Size = new System.Drawing.Size(56, 51);
            pctIcon.TabIndex = 3;
            pctIcon.TabStop = false;

            MESSAGEForm.StartPosition = FormStartPosition.CenterScreen;

            MESSAGEForm.ShowIcon = true;
            MESSAGEForm.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            MESSAGEForm.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            MESSAGEForm.BackColor = System.Drawing.Color.Gainsboro;
            MESSAGEForm.ClientSize = new System.Drawing.Size(380, 168);
            MESSAGEForm.Controls.Add(pctIcon);
            MESSAGEForm.Controls.Add(tbMessage);
            MESSAGEForm.Controls.Add(btnSave);
            MESSAGEForm.Controls.Add(btnCancel);
            MESSAGEForm.ForeColor = System.Drawing.SystemColors.ControlText;
            MESSAGEForm.FormBorderStyle = FormBorderStyle.FixedDialog;
            MESSAGEForm.MaximizeBox = false;
            MESSAGEForm.MinimizeBox = false;
            MESSAGEForm.Name = "MESSAGEForm";
            ((System.ComponentModel.ISupportInitialize)(pctIcon)).EndInit();
            MESSAGEForm.ResumeLayout(false);
            MESSAGEForm.PerformLayout();
        }

        private void showRetryCancel()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(MessageBOX));
            this.btnRetry = new System.Windows.Forms.Button();
            this.btnCancel = new System.Windows.Forms.Button();
            this.tbMessage = new System.Windows.Forms.RichTextBox();
            this.pctIcon = new System.Windows.Forms.PictureBox();
            ((System.ComponentModel.ISupportInitialize)(this.pctIcon)).BeginInit();
            MESSAGEForm.SuspendLayout();

            //btnRetry
            btnRetry.Location = new System.Drawing.Point(82, 133);
            btnRetry.Name = "btnRetry";
            btnRetry.Size = new System.Drawing.Size(75, 23);
            btnRetry.TabIndex = 0;
            btnRetry.UseVisualStyleBackColor = true;
            btnRetry.Text = "Retry";
            btnRetry.DialogResult = DialogResult.Retry;


            //btnCancel
            btnCancel.Location = new System.Drawing.Point(224, 133);
            btnCancel.Name = "btnCancel";
            btnCancel.Size = new System.Drawing.Size(75, 23);
            btnCancel.TabIndex = 0;
            btnCancel.UseVisualStyleBackColor = true;
            btnCancel.Text = "Cancel";
            btnCancel.DialogResult = DialogResult.Cancel;
            //btnCancel.Click += new EventHandler(btnCancel_Click);

            //tbMessage
            tbMessage.BackColor = System.Drawing.Color.Gainsboro;
            tbMessage.BorderStyle = System.Windows.Forms.BorderStyle.None;
            tbMessage.Cursor = System.Windows.Forms.Cursors.Arrow;
            tbMessage.Font = new System.Drawing.Font("Gadugi", 9.5F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            tbMessage.Location = new System.Drawing.Point(90, 46);
            tbMessage.Size = new System.Drawing.Size(278, 56);
            tbMessage.Multiline = true;
            tbMessage.Name = "tbMessage";
            tbMessage.ReadOnly = true;
            tbMessage.ShortcutsEnabled = false;
            tbMessage.TabIndex = 2;
            tbMessage.SelectAll();
            tbMessage.SelectionAlignment = System.Windows.Forms.HorizontalAlignment.Center;
            tbMessage.DeselectAll();
            tbMessage.DoubleClick += new EventHandler(tbMessage_DoubleClick);

            //pctIcon
            pctIcon.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Center;
            pctIcon.Location = new System.Drawing.Point(22, 43);
            pctIcon.Name = "pctIcon";
            pctIcon.Size = new System.Drawing.Size(56, 51);
            pctIcon.TabIndex = 3;
            pctIcon.TabStop = false;

            MESSAGEForm.StartPosition = FormStartPosition.CenterScreen;

            MESSAGEForm.ShowIcon = true;
            MESSAGEForm.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            MESSAGEForm.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            MESSAGEForm.BackColor = System.Drawing.Color.Gainsboro;
            MESSAGEForm.ClientSize = new System.Drawing.Size(380, 168);
            MESSAGEForm.Controls.Add(pctIcon);
            MESSAGEForm.Controls.Add(tbMessage);
            MESSAGEForm.Controls.Add(btnRetry);
            MESSAGEForm.Controls.Add(btnCancel);
            MESSAGEForm.ForeColor = System.Drawing.SystemColors.ControlText;
            MESSAGEForm.FormBorderStyle = FormBorderStyle.FixedDialog;
            MESSAGEForm.MaximizeBox = false;
            MESSAGEForm.MinimizeBox = false;
            MESSAGEForm.Name = "MESSAGEForm";
            ((System.ComponentModel.ISupportInitialize)(pctIcon)).EndInit();
            MESSAGEForm.ResumeLayout(false);
            MESSAGEForm.PerformLayout();
        }

        private void showAbortRetryIgnore()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(MessageBOX));
            this.btnAbort = new System.Windows.Forms.Button();
            this.btnRetry = new System.Windows.Forms.Button();
            this.btnIgnore = new System.Windows.Forms.Button();
            this.tbMessage = new System.Windows.Forms.RichTextBox();
            this.pctIcon = new System.Windows.Forms.PictureBox();
            ((System.ComponentModel.ISupportInitialize)(this.pctIcon)).BeginInit();
            MESSAGEForm.SuspendLayout();

            // 
            // btnAbort
            // 
            btnAbort.DialogResult = System.Windows.Forms.DialogResult.OK;
            btnAbort.Location = new System.Drawing.Point(57, 133);
            btnAbort.Name = "btnAbort";
            btnAbort.Size = new System.Drawing.Size(75, 23);
            btnAbort.TabIndex = 0;
            btnAbort.Text = "Abort";
            btnAbort.UseVisualStyleBackColor = true;
            btnAbort.DialogResult = DialogResult.Abort;

            // 
            // btnRetry
            // 
            btnRetry.Location = new System.Drawing.Point(159, 133);
            btnRetry.Name = "btnRetry";
            btnRetry.Size = new System.Drawing.Size(75, 23);
            btnRetry.TabIndex = 1;
            btnRetry.Text = "Retry";
            btnRetry.UseVisualStyleBackColor = true;
            btnRetry.DialogResult = DialogResult.Retry;
            //btnRetry.Click += new System.EventHandler(this.button2_Click);

            // btnIgnore
            // 
            btnIgnore.Location = new System.Drawing.Point(262, 133);
            btnIgnore.Name = "btnIgnore";
            btnIgnore.Size = new System.Drawing.Size(75, 23);
            btnIgnore.TabIndex = 3;
            btnIgnore.Text = "Ignore";
            btnIgnore.UseVisualStyleBackColor = true;
            btnIgnore.DialogResult = DialogResult.Ignore;

            //tbMessage
            tbMessage.BackColor = System.Drawing.Color.Gainsboro;
            tbMessage.BorderStyle = System.Windows.Forms.BorderStyle.None;
            tbMessage.Cursor = System.Windows.Forms.Cursors.Arrow;
            tbMessage.Font = new System.Drawing.Font("Gadugi", 9.5F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            tbMessage.Location = new System.Drawing.Point(90, 46);
            tbMessage.Size = new System.Drawing.Size(278, 56);
            tbMessage.Multiline = true;
            tbMessage.Name = "tbMessage";
            tbMessage.ReadOnly = true;
            tbMessage.ShortcutsEnabled = false;
            tbMessage.TabIndex = 2;
            tbMessage.SelectAll();
            tbMessage.SelectionAlignment = System.Windows.Forms.HorizontalAlignment.Center;
            tbMessage.DeselectAll();
            tbMessage.DoubleClick += new EventHandler(tbMessage_DoubleClick);

            //pctIcon
            pctIcon.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Center;
            pctIcon.Location = new System.Drawing.Point(22, 43);
            pctIcon.Name = "pctIcon";
            pctIcon.Size = new System.Drawing.Size(56, 51);
            pctIcon.TabIndex = 3;
            pctIcon.TabStop = false;

            MESSAGEForm.StartPosition = FormStartPosition.CenterScreen;

            MESSAGEForm.ShowIcon = true;
            MESSAGEForm.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            MESSAGEForm.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            MESSAGEForm.BackColor = System.Drawing.Color.Gainsboro;
            MESSAGEForm.ClientSize = new System.Drawing.Size(380, 168);
            MESSAGEForm.Controls.Add(pctIcon);
            MESSAGEForm.Controls.Add(tbMessage);
            MESSAGEForm.Controls.Add(btnAbort);
            MESSAGEForm.Controls.Add(btnRetry);
            MESSAGEForm.Controls.Add(btnIgnore);
            MESSAGEForm.ForeColor = System.Drawing.SystemColors.ControlText;
            MESSAGEForm.FormBorderStyle = FormBorderStyle.FixedDialog;
            MESSAGEForm.MaximizeBox = false;
            MESSAGEForm.MinimizeBox = false;
            MESSAGEForm.Name = "MESSAGEForm";
            ((System.ComponentModel.ISupportInitialize)(pctIcon)).EndInit();
            MESSAGEForm.ResumeLayout(false);
            MESSAGEForm.PerformLayout();
        }

        public DialogResult getMessageOK(string Code)
        {
            ResourceManager resmanage = new ResourceManager("GenMSG.Resources", refAssembly);

            BRMessage brMessage = new BRMessage();

            BRProp brProp = new BRProp();  ///use to get data

            BRProp brPropRet = new BRProp(); ///use to store return data

            ///consume br function; requirements brproperties
            brPropRet = brMessage.getMsg(brProp, Code);
            //Used if database picture wasn't found
            object error1 = resmanage.GetObject("Error1", new global::System.Globalization.CultureInfo("en-US"));
            object error = resmanage.GetObject("Error", new global::System.Globalization.CultureInfo("en-US"));
            showOK();

            try
            {
                object okicon = resmanage.GetObject(brPropRet.headerMessage + "1", new global::System.Globalization.CultureInfo("en-US"));
                object okicon1 = resmanage.GetObject(brPropRet.headerMessage, new global::System.Globalization.CultureInfo("en-US"));
                MESSAGEForm.Icon = (System.Drawing.Icon)okicon;

                MESSAGEForm.Text = brPropRet.headerMessage;
                tbMessage.Text = brPropRet.bodyMsg;
                pctIcon.Image = (Bitmap)okicon1;
            }
            catch (Exception e)
            {

                MESSAGEForm.Icon = (System.Drawing.Icon)error1;

                MESSAGEForm.Text = "";
                //If database is not connected
                if (brPropRet.status != null)
                {
                    tbMessage.Text = brPropRet.status;
                }
                //If database is connected but there is a null value
                else
                {
                    tbMessage.Text = "Message not found. There is a null value in the database.";
                }

                pctIcon.Image = (Bitmap)error;
            }
            
            //Dialog to display MESSAGEForm
            DialogResult result = MESSAGEForm.ShowDialog();
            //Used to reset controls from the MESSAGEForm after closing
            MESSAGEForm.Controls.Clear();
            return result;
        }

        public DialogResult getMessageYesNo(string Code)
        {
            ResourceManager resmanage = new ResourceManager("GenMSG.Resources", refAssembly);

            BRMessage brMessage = new BRMessage();

            BRProp brProp = new BRProp();  ///use to get data

            BRProp brPropRet = new BRProp(); ///use to store return data

            ///consume br function; requirements brproperties

            brPropRet = brMessage.getMsg(brProp, Code);

            //Used if database picture wasn't found
            object error1 = resmanage.GetObject("Error1", new global::System.Globalization.CultureInfo("en-US"));
            object error = resmanage.GetObject("Error", new global::System.Globalization.CultureInfo("en-US"));

            try
            {
                object okicon = resmanage.GetObject(brPropRet.headerMessage + "1", new global::System.Globalization.CultureInfo("en-US"));
                object okicon1 = resmanage.GetObject(brPropRet.headerMessage, new global::System.Globalization.CultureInfo("en-US"));
                showYesNo();
                MESSAGEForm.Icon = (System.Drawing.Icon)okicon;

                MESSAGEForm.Text = brPropRet.headerMessage;
                tbMessage.Text = brPropRet.bodyMsg;
                pctIcon.Image = (Bitmap)okicon1;
            }
            catch (Exception e)
            {
                showOK();
                MESSAGEForm.Icon = (System.Drawing.Icon)error1;

                MESSAGEForm.Text = "";
                //If database is not connected
                if (brPropRet.status != null)
                {
                    tbMessage.Text = brPropRet.status;
                }
                //If database is connected but there is a null value
                else
                {
                    tbMessage.Text = "Message not found. There is a null value in the database.";
                }
                
                pctIcon.Image = (Bitmap)error;
            }

            //Dialog to display MESSAGEForm
            DialogResult result = MESSAGEForm.ShowDialog();
            //Used to reset controls from the MESSAGEForm after closing
            MESSAGEForm.Controls.Clear();
            return result;
        }

        public DialogResult getMessageYesNoCancel(string Code)
        {
            ResourceManager resmanage = new ResourceManager("GenMSG.Resources", refAssembly);

            BRMessage brMessage = new BRMessage();

            BRProp brProp = new BRProp();  ///use to get data

            BRProp brPropRet = new BRProp(); ///use to store return data

            ///consume br function; requirements brproperties

            brPropRet = brMessage.getMsg(brProp, Code);

            //Used if database picture wasn't found
            object error1 = resmanage.GetObject("Error1", new global::System.Globalization.CultureInfo("en-US"));
            object error = resmanage.GetObject("Error", new global::System.Globalization.CultureInfo("en-US"));

            try
            {
                object okicon = resmanage.GetObject(brPropRet.headerMessage + "1", new global::System.Globalization.CultureInfo("en-US"));
                object okicon1 = resmanage.GetObject(brPropRet.headerMessage, new global::System.Globalization.CultureInfo("en-US"));
                showYesNoCancel();
                MESSAGEForm.Icon = (System.Drawing.Icon)okicon;

                MESSAGEForm.Text = brPropRet.headerMessage;
                tbMessage.Text = brPropRet.bodyMsg;
                pctIcon.Image = (Bitmap)okicon1;
            }
            catch (Exception)
            {
                showOK();
                MESSAGEForm.Icon = (System.Drawing.Icon)error1;

                MESSAGEForm.Text = "";
                //If database is not connected
                if (brPropRet.status != null)
                {
                    tbMessage.Text = brPropRet.status;
                }
                //If database is connected but there is a null value
                else
                {
                    tbMessage.Text = "Message not found. There is a null value in the database.";
                }
                
                pctIcon.Image = (Bitmap)error;
            }

            //Dialog to display MESSAGEForm
            DialogResult result = MESSAGEForm.ShowDialog();
            //Used to reset controls from the MESSAGEForm after closing
            MESSAGEForm.Controls.Clear();
            return result;
        }

        public DialogResult getMessageOKCancel(string Code)
        {
            ResourceManager resmanage = new ResourceManager("GenMSG.Resources", refAssembly);

            BRMessage brMessage = new BRMessage();

            BRProp brProp = new BRProp();  ///use to get data

            BRProp brPropRet = new BRProp(); ///use to store return data

            ///consume br function; requirements brproperties

            brPropRet = brMessage.getMsg(brProp, Code);

            //Used if database picture wasn't found
            object error1 = resmanage.GetObject("Error1", new global::System.Globalization.CultureInfo("en-US"));
            object error = resmanage.GetObject("Error", new global::System.Globalization.CultureInfo("en-US"));

            try
            {
                object okicon = resmanage.GetObject(brPropRet.headerMessage + "1", new global::System.Globalization.CultureInfo("en-US"));
                object okicon1 = resmanage.GetObject(brPropRet.headerMessage, new global::System.Globalization.CultureInfo("en-US"));
                showOKCancel();
                MESSAGEForm.Icon = (System.Drawing.Icon)okicon;

                MESSAGEForm.Text = brPropRet.headerMessage;
                tbMessage.Text = brPropRet.bodyMsg;
                pctIcon.Image = (Bitmap)okicon1;
            }
            catch (Exception e)
            {
                showOK();
                MESSAGEForm.Icon = (System.Drawing.Icon)error1;

                MESSAGEForm.Text = "";
                //If database is not connected
                if (brPropRet.status != null)
                {
                    tbMessage.Text = brPropRet.status;
                }
                //If database is connected but there is a null value
                else
                {
                    tbMessage.Text = "Message not found. There is a null value in the database.";
                }
                
                pctIcon.Image = (Bitmap)error;

            }

            //Dialog to display MESSAGEForm
            DialogResult result = MESSAGEForm.ShowDialog();
            //Used to reset controls from the MESSAGEForm after closing
            MESSAGEForm.Controls.Clear();
            return result;
        }

        public DialogResult getMessageSaveCancel(string Code)
        {
            ResourceManager resmanage = new ResourceManager("GenMSG.Resources", refAssembly);

            BRMessage brMessage = new BRMessage();

            BRProp brProp = new BRProp();  ///use to get data

            BRProp brPropRet = new BRProp(); ///use to store return data

            ///consume br function; requirements brproperties

            brPropRet = brMessage.getMsg(brProp, Code);

            //Used if database picture wasn't found
            object error1 = resmanage.GetObject("Error1", new global::System.Globalization.CultureInfo("en-US"));
            object error = resmanage.GetObject("Error", new global::System.Globalization.CultureInfo("en-US"));


            try
            {
                object okicon = resmanage.GetObject(brPropRet.headerMessage + "1", new global::System.Globalization.CultureInfo("en-US"));
                object okicon1 = resmanage.GetObject(brPropRet.headerMessage, new global::System.Globalization.CultureInfo("en-US"));
                showSaveCancel();
                MESSAGEForm.Icon = (System.Drawing.Icon)okicon;

                MESSAGEForm.Text = brPropRet.headerMessage;
                tbMessage.Text = brPropRet.bodyMsg;
                pctIcon.Image = (Bitmap)okicon1;

            }
            catch (Exception e)
            {
                showOK();
                MESSAGEForm.Icon = (System.Drawing.Icon)error1;

                MESSAGEForm.Text = "";
                //If database is not connected
                if (brPropRet.status != null)
                {
                    tbMessage.Text = brPropRet.status;
                }
                //If database is connected but there is a null value
                else
                {
                    tbMessage.Text = "Message not found. There is a null value in the database.";
                }
                
                pctIcon.Image = (Bitmap)error;
            }


            //Dialog to display MESSAGEForm
            DialogResult result = MESSAGEForm.ShowDialog();
            //Used to reset controls from the MESSAGEForm after closing
            MESSAGEForm.Controls.Clear();
            return result;
        }

        public DialogResult getMessageRetryCancel(string Code)
        {
            ResourceManager resmanage = new ResourceManager("GenMSG.Resources", refAssembly);

            BRMessage brMessage = new BRMessage();

            BRProp brProp = new BRProp();  ///use to get data

            BRProp brPropRet = new BRProp(); ///use to store return data

            ///consume br function; requirements brproperties

            brPropRet = brMessage.getMsg(brProp, Code);

            //Used if database picture wasn't found
            object error1 = resmanage.GetObject("Error1", new global::System.Globalization.CultureInfo("en-US"));
            object error = resmanage.GetObject("Error", new global::System.Globalization.CultureInfo("en-US"));

            try
            {
                object okicon = resmanage.GetObject(brPropRet.headerMessage + "1", new global::System.Globalization.CultureInfo("en-US"));
                object okicon1 = resmanage.GetObject(brPropRet.headerMessage, new global::System.Globalization.CultureInfo("en-US"));
                showRetryCancel();
                MESSAGEForm.Icon = (System.Drawing.Icon)okicon;

                MESSAGEForm.Text = brPropRet.headerMessage;
                tbMessage.Text = brPropRet.bodyMsg;
                pctIcon.Image = (Bitmap)okicon1;
            }
            catch (Exception e)
            {
                showOK();
                MESSAGEForm.Icon = (System.Drawing.Icon)error1;

                MESSAGEForm.Text = "";
                //If database is not connected
                if (brPropRet.status != null)
                {
                    tbMessage.Text = brPropRet.status;
                }
                //If database is connected but there is a null value
                else
                {
                    tbMessage.Text = "Message not found. There is a null value in the database.";
                }
                
                pctIcon.Image = (Bitmap)error;
            }

            //Dialog to display MESSAGEForm
            DialogResult result = MESSAGEForm.ShowDialog();
            //Used to reset controls from the MESSAGEForm after closing
            MESSAGEForm.Controls.Clear();
            return result;
        }

        public DialogResult getMessageAbortRetryIgnore(string Code)
        {
            ResourceManager resmanage = new ResourceManager("GenMSG.Resources", refAssembly);

            BRMessage brMessage = new BRMessage();

            BRProp brProp = new BRProp();  ///use to get data

            BRProp brPropRet = new BRProp(); ///use to store return data

            ///consume br function; requirements brproperties

            brPropRet = brMessage.getMsg(brProp, Code);

            //Used if database picture wasn't found
            object error1 = resmanage.GetObject("Error1", new global::System.Globalization.CultureInfo("en-US"));
            object error = resmanage.GetObject("Error", new global::System.Globalization.CultureInfo("en-US"));

            try
            {
                object okicon = resmanage.GetObject(brPropRet.headerMessage + "1", new global::System.Globalization.CultureInfo("en-US"));
                object okicon1 = resmanage.GetObject(brPropRet.headerMessage, new global::System.Globalization.CultureInfo("en-US"));
                showAbortRetryIgnore();
                MESSAGEForm.Icon = (System.Drawing.Icon)okicon;

                MESSAGEForm.Text = brPropRet.headerMessage;
                tbMessage.Text = brPropRet.bodyMsg;
                pctIcon.Image = (Bitmap)okicon1;
            }
            catch (Exception e)
            {
                showOK();
                MESSAGEForm.Icon = (System.Drawing.Icon)error1;

                MESSAGEForm.Text = "";
                //If database is not connected
                if (brPropRet.status != null)
                {
                    tbMessage.Text = brPropRet.status;
                }
                //If database is connected but there is a null value
                else
                {
                    tbMessage.Text = "Message not found. There is a null value in the database.";
                }
               
                pctIcon.Image = (Bitmap)error;
            }

            //Dialog to display MESSAGEForm
            DialogResult result = MESSAGEForm.ShowDialog();
            //Used to reset controls from the MESSAGEForm after closing
            MESSAGEForm.Controls.Clear();
            return result;
        }


        private void tbMessage_DoubleClick(object sender, EventArgs e)
        {
            tbMessage.DeselectAll();
        }

        ////when OK button is clicked
        private void btnOK_Click(object sender, EventArgs e)
        {
            MESSAGEForm.Close();
        }
        

    }
}
