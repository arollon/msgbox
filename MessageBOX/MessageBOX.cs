﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Windows.Forms;
using System.Resources;
using System.IO;
using System.Runtime.InteropServices;
using MsgBox;

namespace MsgBox
{
    public class MessageBOX
    {
        //BRProp BrRET = new BRProp();
        //BRProp BrProp = new BRProp();
        //BRMessage BRP = new BRMessage();
        public Form MESSAGEForm = new Form();
        public PictureBox pctIcon = new PictureBox();
        public RichTextBox tbMessage = new RichTextBox();
        public Button btnOK;
        public Button btnCancel = new Button();
        public Button btnYes = new Button();
        public Button btnNo = new Button();
        public BtnType buttonType;
        public MsgType messageType;
        DialogType dialogType = new DialogType();
        private static Mutex mutex = new Mutex();
        public bool boolYN { get; private set; }
        int Count { get; }

        

        //public string getMSG(string headertxt, string msgtxt)
        //{
        //    BrProp.headerMessage = headertxt;
        //    BrProp.bodyMsg = msgtxt;
        //    return BrRET.Message;
        //}

        private void showOK()
        {
            mutex.WaitOne();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(MessageBOX));
            this.btnOK = new System.Windows.Forms.Button();
            this.tbMessage = new System.Windows.Forms.RichTextBox();
            this.pctIcon = new System.Windows.Forms.PictureBox();
            ((System.ComponentModel.ISupportInitialize)(this.pctIcon)).BeginInit();
            MESSAGEForm.SuspendLayout();

            //btnOK
            btnOK.DialogResult = DialogResult.OK;
            btnOK.Location = new System.Drawing.Point(159, 133);
            btnOK.Name = "btnOK";
            btnOK.Size = new System.Drawing.Size(75, 23);
            btnOK.TabIndex = 0;
            btnOK.UseVisualStyleBackColor = true;
            btnOK.Text = "OK";
            btnOK.Click += new EventHandler(btnOK_Click);

            //tbMessage
            tbMessage.BackColor = System.Drawing.Color.Gainsboro;
            tbMessage.BorderStyle = System.Windows.Forms.BorderStyle.None;
            tbMessage.Cursor = System.Windows.Forms.Cursors.Arrow;
            tbMessage.Font = new System.Drawing.Font("Gadugi", 9.5F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            tbMessage.Location = new System.Drawing.Point(90, 46);
            tbMessage.Size = new System.Drawing.Size(278, 56);
            tbMessage.Multiline = true;
            tbMessage.Name = "tbMessage";
            tbMessage.ReadOnly = true;
            tbMessage.ShortcutsEnabled = false;
            tbMessage.TabIndex = 2;
            tbMessage.SelectionAlignment = System.Windows.Forms.HorizontalAlignment.Center;
            tbMessage.DoubleClick += new EventHandler(tbMessage_DoubleClick);

            //pctIcon
            pctIcon.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Center;
            pctIcon.Location = new System.Drawing.Point(22, 43);
            pctIcon.Name = "pctIcon";
            pctIcon.Size = new System.Drawing.Size(56, 51);
            pctIcon.TabIndex = 3;
            pctIcon.TabStop = false;

            MESSAGEForm.StartPosition = FormStartPosition.CenterScreen;
            MESSAGEForm.Show();
            MESSAGEForm.ShowIcon = true;
            MESSAGEForm.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            MESSAGEForm.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            MESSAGEForm.BackColor = System.Drawing.Color.Gainsboro;
            MESSAGEForm.ClientSize = new System.Drawing.Size(380, 168);
            MESSAGEForm.Controls.Add(pctIcon);
            MESSAGEForm.Controls.Add(tbMessage);
            MESSAGEForm.Controls.Add(btnOK);
            MESSAGEForm.ForeColor = System.Drawing.SystemColors.ControlText;
            MESSAGEForm.FormBorderStyle = FormBorderStyle.FixedDialog;
            MESSAGEForm.MaximizeBox = false;
            MESSAGEForm.MinimizeBox = false;
            MESSAGEForm.Name = "MESSAGEForm";
            ((System.ComponentModel.ISupportInitialize)(pctIcon)).EndInit();
            MESSAGEForm.ResumeLayout(false);
            MESSAGEForm.PerformLayout();
        }

        private void showYesNo()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(MessageBOX));
            this.btnOK = new System.Windows.Forms.Button();
            this.tbMessage = new System.Windows.Forms.RichTextBox();
            this.pctIcon = new System.Windows.Forms.PictureBox();
            ((System.ComponentModel.ISupportInitialize)(this.pctIcon)).BeginInit();
            MESSAGEForm.SuspendLayout();

            //btnYes
            btnYes.DialogResult = DialogResult.Yes;
            btnYes.Location = new System.Drawing.Point(82, 133);
            btnYes.Name = "btnYES";
            btnYes.Size = new System.Drawing.Size(75, 23);
            btnYes.TabIndex = 0;
            btnYes.UseVisualStyleBackColor = true;
            btnYes.Text = "Yes";

            //btnNo
            btnNo.DialogResult = DialogResult.No;
            btnNo.Location = new System.Drawing.Point(224, 133);
            btnNo.Name = "btnNO";
            btnNo.Size = new System.Drawing.Size(75, 23);
            btnNo.TabIndex = 0;
            btnNo.UseVisualStyleBackColor = true;
            btnNo.Text = "No";

            //tbMessage
            tbMessage.BackColor = System.Drawing.Color.Gainsboro;
            tbMessage.BorderStyle = System.Windows.Forms.BorderStyle.None;
            tbMessage.Cursor = System.Windows.Forms.Cursors.Arrow;
            tbMessage.Font = new System.Drawing.Font("Gadugi", 9.5F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            tbMessage.Location = new System.Drawing.Point(90, 46);
            tbMessage.Size = new System.Drawing.Size(278, 56);
            tbMessage.Multiline = true;
            tbMessage.Name = "tbMessage";
            tbMessage.ReadOnly = true;
            tbMessage.ShortcutsEnabled = false;
            tbMessage.TabIndex = 2;
            tbMessage.SelectAll();
            tbMessage.SelectionAlignment = System.Windows.Forms.HorizontalAlignment.Center;
            tbMessage.DeselectAll();
            tbMessage.DoubleClick += new EventHandler(tbMessage_DoubleClick);

            //pctIcon
            pctIcon.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Center;
            pctIcon.Location = new System.Drawing.Point(22, 43);
            pctIcon.Name = "pctIcon";
            pctIcon.Size = new System.Drawing.Size(56, 51);
            pctIcon.TabIndex = 3;
            pctIcon.TabStop = false;

            MESSAGEForm.StartPosition = FormStartPosition.CenterScreen;
            MESSAGEForm.Show();
            MESSAGEForm.ShowIcon = true;
            MESSAGEForm.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            MESSAGEForm.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            MESSAGEForm.BackColor = System.Drawing.Color.Gainsboro;
            MESSAGEForm.ClientSize = new System.Drawing.Size(380, 168);
            MESSAGEForm.Controls.Add(pctIcon);
            MESSAGEForm.Controls.Add(tbMessage);
            MESSAGEForm.Controls.Add(btnYes);
            MESSAGEForm.Controls.Add(btnNo);
            MESSAGEForm.ForeColor = System.Drawing.SystemColors.ControlText;
            MESSAGEForm.FormBorderStyle = FormBorderStyle.FixedDialog;
            MESSAGEForm.MaximizeBox = false;
            MESSAGEForm.MinimizeBox = false;
            MESSAGEForm.Name = "MESSAGEForm";
            ((System.ComponentModel.ISupportInitialize)(pctIcon)).EndInit();
            MESSAGEForm.ResumeLayout(false);
            MESSAGEForm.PerformLayout();
        }

        private void showOKCancel()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(MessageBOX));
            this.btnOK = new System.Windows.Forms.Button();
            this.tbMessage = new System.Windows.Forms.RichTextBox();
            this.pctIcon = new System.Windows.Forms.PictureBox();
            ((System.ComponentModel.ISupportInitialize)(this.pctIcon)).BeginInit();
            MESSAGEForm.SuspendLayout();

            //btnOK
            btnOK.DialogResult = DialogResult.OK;
            btnOK.Location = new System.Drawing.Point(82, 133);
            btnOK.Name = "btnOK";
            btnOK.Size = new System.Drawing.Size(75, 23);
            btnOK.TabIndex = 0;
            btnOK.UseVisualStyleBackColor = true;
            btnOK.Text = "OK";
            btnOK.Click += new EventHandler(btnOK_Click);

            //btnCancel
            btnCancel.DialogResult = DialogResult.Cancel;
            btnCancel.Location = new System.Drawing.Point(224, 133);
            btnCancel.Name = "btnCancel";
            btnCancel.Size = new System.Drawing.Size(75, 23);
            btnCancel.TabIndex = 0;
            btnCancel.UseVisualStyleBackColor = true;
            btnCancel.Text = "Cancel";
            btnCancel.Click += new EventHandler(btnCancel_Click);

            //tbMessage
            tbMessage.BackColor = System.Drawing.Color.Gainsboro;
            tbMessage.BorderStyle = System.Windows.Forms.BorderStyle.None;
            tbMessage.Cursor = System.Windows.Forms.Cursors.Arrow;
            tbMessage.Font = new System.Drawing.Font("Gadugi", 9.5F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            tbMessage.Location = new System.Drawing.Point(90, 46);
            tbMessage.Size = new System.Drawing.Size(278, 56);
            tbMessage.Multiline = true;
            tbMessage.Name = "tbMessage";
            tbMessage.ReadOnly = true;
            tbMessage.ShortcutsEnabled = false;
            tbMessage.TabIndex = 2;
            tbMessage.SelectAll();
            tbMessage.SelectionAlignment = System.Windows.Forms.HorizontalAlignment.Center;
            tbMessage.DeselectAll();
            tbMessage.DoubleClick += new EventHandler(tbMessage_DoubleClick);

            //pctIcon
            pctIcon.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Center;
            pctIcon.Location = new System.Drawing.Point(22, 43);
            pctIcon.Name = "pctIcon";
            pctIcon.Size = new System.Drawing.Size(56, 51);
            pctIcon.TabIndex = 3;
            pctIcon.TabStop = false;

            MESSAGEForm.StartPosition = FormStartPosition.CenterScreen;
            MESSAGEForm.Show();
            MESSAGEForm.ShowIcon = true;
            MESSAGEForm.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            MESSAGEForm.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            MESSAGEForm.BackColor = System.Drawing.Color.Gainsboro;
            MESSAGEForm.ClientSize = new System.Drawing.Size(380, 168);
            MESSAGEForm.Controls.Add(pctIcon);
            MESSAGEForm.Controls.Add(tbMessage);
            MESSAGEForm.Controls.Add(btnOK);
            MESSAGEForm.Controls.Add(btnCancel);
            MESSAGEForm.ForeColor = System.Drawing.SystemColors.ControlText;
            MESSAGEForm.FormBorderStyle = FormBorderStyle.FixedDialog;
            MESSAGEForm.MaximizeBox = false;
            MESSAGEForm.MinimizeBox = false;
            MESSAGEForm.Name = "MESSAGEForm";
            ((System.ComponentModel.ISupportInitialize)(pctIcon)).EndInit();
            MESSAGEForm.ResumeLayout(false);
            MESSAGEForm.PerformLayout();
        }


        public void msgOK(string msgtxt, MsgType msgType, BtnType btnType) 
        {

            BRMessage brMessage = new BRMessage();

            BRProp brProp = new BRProp();  ///use to get data

            BRProp brPropRet = new BRProp(); ///use to store return data

            ///set all properties needed
            brProp.bodyMsg = msgtxt;

            ///consume br function; requirments brproperties
            brPropRet = brMessage.getMsg(brProp, msgType, btnType);

            //Button Text
            if (btnType == BtnType.Ok)
            {
                showOK();
            }
            if (btnType == BtnType.OkCancel)
            {
                showOKCancel();
                btnCancel.Text = DialogType.Cancel.ToString();
            }
            if (btnType == BtnType.YesNo)
            {
                showYesNo();
                btnYes.Text = DialogType.Yes.ToString();
               
            }

            //Header Text
            MESSAGEForm.Text = brPropRet.Message;
            //Message Text
            tbMessage.Text = brPropRet.code + msgtxt ;

            pctIcon.Image = Image.FromFile(brPropRet.icon + msgType + ".png");
            MESSAGEForm.Icon = new Icon(brPropRet.icon + msgType + ".ico");
        }

        public void msgOKCancel(string headertxt, string msgtxt, string Code, MsgType msgType, BtnType btnType)
        {
            BRMessage brMessage = new BRMessage();

            BRProp brProp = new BRProp();  ///use to get data

            BRProp brPropRet = new BRProp(); ///use to store return data

            ///set all properties needed
            brProp.headerMessage = headertxt;
            brProp.bodyMsg = msgtxt;
            brProp.code = Code;

            ///consume br function; requirments brproperties
            brPropRet = brMessage.getMsg(brProp, msgType, btnType);

            showYesNo();

            //Button Text

            //Header Text
            MESSAGEForm.Text = brPropRet.Message;
            //Message Text
            tbMessage.Text = msgtxt;

            pctIcon.Image = Image.FromFile(brPropRet.icon + msgType + ".png");
            MESSAGEForm.Icon = new Icon(brPropRet.icon + msgType + ".ico");
        }

        private void tbMessage_DoubleClick(object sender, EventArgs e)
        {
            tbMessage.DeselectAll();
        }

        ////when OK button is clicked
        private void btnOK_Click(object sender, EventArgs e)
        {

           

        }

        ////when Cancel button is clicked
        private void btnCancel_Click(object sender, EventArgs e)
        {
            MESSAGEForm.Close();
        }

        ////when Yes button is clicked
        private void btnYes_Click(object sender, EventArgs e)
        {
           
        }
        
        ////when No button is clicked
        private void btnNo_Click(object sender, EventArgs e)
        {

        }

    }
}
